package com.example.StoreOnlineRestAPI.Entity;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Table(name = "address_types")
public class AddressType extends BaseEntityName implements Serializable {
   
}
