package com.example.StoreOnlineRestAPI.Entity;
import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name="deliveries")
public class Delivery extends BaseEntityName implements Serializable {
   
}
