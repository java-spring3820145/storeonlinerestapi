package com.example.StoreOnlineRestAPI;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@SpringBootApplication
public class StoreOnlineRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoreOnlineRestApiApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public OpenAPI openAPI() {
		Info info = new Info();
		info.setTitle("Rest API Store Online");
		info.setDescription("Documentation Rest API Store Online v.1");
		info.setVersion("v.1");
		return new OpenAPI().info(info);
	}

}
