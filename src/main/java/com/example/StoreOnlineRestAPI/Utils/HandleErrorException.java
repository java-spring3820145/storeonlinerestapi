package com.example.StoreOnlineRestAPI.Utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class HandleErrorException {
    public static List<String> parse(Errors errors){
        List<String> messages=new ArrayList<>();
        for(ObjectError error:errors.getFieldErrors()){
            System.out.println("error "+error);
            messages.add(error.getDefaultMessage());
        }
        return messages;
    }
}
