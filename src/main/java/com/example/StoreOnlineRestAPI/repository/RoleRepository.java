package com.example.StoreOnlineRestAPI.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.StoreOnlineRestAPI.Entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {

     Page<Role> findByNameContains(String name, Pageable pageable);
    
} 
