package com.example.StoreOnlineRestAPI.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.StoreOnlineRestAPI.Entity.Division;

@Repository
public interface DivisionRepository extends JpaRepository<Division,Integer>{
      Page<Division> findByNameContains(String name, Pageable pageable);
}
