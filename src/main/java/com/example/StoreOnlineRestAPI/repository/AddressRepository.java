package com.example.StoreOnlineRestAPI.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.StoreOnlineRestAPI.Entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, String> {
    Page<Address> findByNameContains(String name, Pageable pageable);

}
