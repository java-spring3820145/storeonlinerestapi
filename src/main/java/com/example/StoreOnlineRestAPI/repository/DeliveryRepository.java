package com.example.StoreOnlineRestAPI.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.StoreOnlineRestAPI.Entity.Delivery;

@Repository
public interface DeliveryRepository extends JpaRepository<Delivery,Integer>{
      Page<Delivery> findByNameContains(String name, Pageable pageable);
}
