package com.example.StoreOnlineRestAPI.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.StoreOnlineRestAPI.Entity.AddressType;

@Repository
public interface AddressTypeRepository extends JpaRepository<AddressType, Integer> {
    Page<AddressType> findByNameContains(String name, Pageable pageable);

}
