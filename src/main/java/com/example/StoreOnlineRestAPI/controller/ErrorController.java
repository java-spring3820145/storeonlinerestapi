package com.example.StoreOnlineRestAPI.controller;

import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import com.example.StoreOnlineRestAPI.model.ResponseData;

@RestControllerAdvice
public class ErrorController {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ResponseData<String>> constraintViolationException(ConstraintViolationException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(
                        ResponseData.<String>builder()
                                .message(exception.getMessage())
                                .build());
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ResponseData<String>> apiException(ResponseStatusException exception) {
        return ResponseEntity.status(exception.getStatusCode())
                .body(ResponseData.<String>builder().message(exception.getReason()).build());
    }
}
