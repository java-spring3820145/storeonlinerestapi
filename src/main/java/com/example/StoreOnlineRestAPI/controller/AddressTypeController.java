package com.example.StoreOnlineRestAPI.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.StoreOnlineRestAPI.Entity.AddressType;
import com.example.StoreOnlineRestAPI.model.AddressTypeRequest;
import com.example.StoreOnlineRestAPI.model.AddressTypeResponse;
import com.example.StoreOnlineRestAPI.model.ResponseData;
import com.example.StoreOnlineRestAPI.model.ResponseDataPaging;
import com.example.StoreOnlineRestAPI.service.AddressTypeService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@RestController
@CrossOrigin(origins = "localhost:7070")
@RequestMapping("/api/address-types")
@Tag(name = "Address Types", description = "Data Address Types")
public class AddressTypeController {
    @Autowired
    private AddressTypeService addressTypeService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("")
    public ResponseEntity<ResponseDataPaging<List<AddressType>>> getData(
            @RequestParam(required = false, defaultValue = "") String keyword,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit) {

        ResponseDataPaging<List<AddressType>> responseData = new ResponseDataPaging<>();

        try {
            Pageable pageable = PageRequest.of(page - 1, limit);
            Page<AddressType> data = addressTypeService.findPagingByParams(keyword, pageable);

            responseData.setStatus(true);
            responseData.setData(data.getContent());
            responseData.setCount(data.getNumberOfElements());
            responseData.setCurrentPage(page);
            responseData.setTotalPage(data.getTotalPages());
            return new ResponseEntity<>(responseData, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AddressType> getDataById(@PathVariable("id") Integer id) {
        try {
            AddressType data = addressTypeService.findById(id);
            if (data != null)
                return new ResponseEntity<AddressType>(data, HttpStatus.OK);
            else
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }
    }

    @PostMapping()
    public ResponseEntity<ResponseData<AddressTypeResponse>> create(
            @Valid @RequestBody AddressTypeRequest addresTypeRequest,
            Errors errors) {
        ResponseData<AddressTypeResponse> responseData = new ResponseData<>();
        try {
            if (errors.hasErrors()) {
                responseData.setStatus(false);
                responseData.setData(null);
                for (ObjectError error : errors.getAllErrors()) {
                    responseData.setMessage(error.getDefaultMessage());
                }
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
            }
            AddressType addressType = modelMapper.map(addresTypeRequest, AddressType.class);
            addressType = addressTypeService.save(addressType);

            AddressTypeResponse data = modelMapper.map(addressType, AddressTypeResponse.class);

            responseData.setStatus(true);
            responseData.setData(data);
            responseData.setMessage("Data berhasil disimpan");
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddressType> update(@PathVariable("id") Integer id,
            @Valid @RequestBody AddressTypeRequest addressTypeRequest) {
        try {
            AddressType addressType = addressTypeService.findById(id);

            if (addressType != null) {
                addressType.setName(addressTypeRequest.getName());
                addressTypeService.save(addressType);
                return new ResponseEntity<AddressType>(addressType, HttpStatus.OK);

            } else
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") Integer id) {
        try {
            addressTypeService.removeOne(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
