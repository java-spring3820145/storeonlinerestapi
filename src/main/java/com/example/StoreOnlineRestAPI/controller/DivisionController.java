package com.example.StoreOnlineRestAPI.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.StoreOnlineRestAPI.Entity.Division;
import com.example.StoreOnlineRestAPI.model.ResponseDataPaging;
import com.example.StoreOnlineRestAPI.model.DivisionRequest;
import com.example.StoreOnlineRestAPI.model.DivisionResponse;
import com.example.StoreOnlineRestAPI.repository.DivisionRepository;
import com.example.StoreOnlineRestAPI.service.DivisionService;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@CrossOrigin(origins = "http://localhost:7070")
@RestController()
@RequestMapping("/api/divisions")
@Tag(name = "Divisions", description = "Data divisions")
public class DivisionController {

    @Autowired
    private DivisionService divisionService;

    @Autowired
    private DivisionRepository divisionRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping()
    public ResponseEntity<ResponseDataPaging<List<Division>>> getData(
            @RequestParam(required = false, defaultValue = "") String keyword,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit) {
        ResponseDataPaging<List<Division>> responseData = new ResponseDataPaging<>();
        try {
            Pageable pageable = PageRequest.of(page - 1, limit);
            Page<Division> data = divisionService.findPagingByParams(keyword, pageable);

            responseData.setStatus(true);
            responseData.setData(data.getContent());
            responseData.setCount(data.getNumberOfElements());
            responseData.setCurrentPage(page);
            responseData.setTotalPage(data.getTotalPages());
            return new ResponseEntity<>(responseData, HttpStatus.OK);
        } catch (Exception e) {
            responseData.setStatus(false);
            responseData.setData(null);
            responseData.setCount(0);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    public ResponseEntity<DivisionResponse> create(@Valid @RequestBody DivisionRequest divisionRequest) {
        Division division = modelMapper.map(divisionRequest, Division.class);
        try {
            Division _division = divisionService
                    .save(division);
            DivisionResponse divisionSuccess = modelMapper.map(_division, DivisionResponse.class);
            return new ResponseEntity<>(divisionSuccess, HttpStatus.CREATED);
        } catch (Exception e) {
            DivisionResponse divisionFailed = modelMapper.map(divisionRequest, DivisionResponse.class);
            return new ResponseEntity<>(divisionFailed, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Division> update(@PathVariable("id") Integer id, @Valid @RequestBody DivisionRequest divisionRequest) {
        Division data = divisionService.findById(id);
        if (data != null) {
            return new ResponseEntity<>(divisionRepository.save(data), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> deleteDivision(@PathVariable("id") Integer id) {
        try {
            divisionRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
