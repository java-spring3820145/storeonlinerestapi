package com.example.StoreOnlineRestAPI.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.StoreOnlineRestAPI.Entity.Delivery;
import com.example.StoreOnlineRestAPI.model.ResponseDataPaging;
import com.example.StoreOnlineRestAPI.model.DeliveryRequest;
import com.example.StoreOnlineRestAPI.model.DeliveryResponse;
import com.example.StoreOnlineRestAPI.repository.DeliveryRepository;
import com.example.StoreOnlineRestAPI.service.DeliveryService;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@CrossOrigin(origins = "http://localhost:7070")
@RestController()
@RequestMapping("/api/deliveries")
@Tag(name = "Deliveries", description = "Data deliveries")
public class DeliveryController {
    
    @Autowired
    private DeliveryService deliveryService;

    @Autowired
    private DeliveryRepository deliveryRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping()
    public ResponseEntity<ResponseDataPaging<List<Delivery>>> getData(
            @RequestParam(required = false, defaultValue = "") String keyword,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit) {
        ResponseDataPaging<List<Delivery>> responseData = new ResponseDataPaging<>();
        try {
            Pageable pageable = PageRequest.of(page - 1, limit);
            Page<Delivery> data = deliveryService.findPagingByParams(keyword, pageable);

            responseData.setStatus(true);
            responseData.setData(data.getContent());
            responseData.setCount(data.getNumberOfElements());
            responseData.setCurrentPage(page);
            responseData.setTotalPage(data.getTotalPages());
            return new ResponseEntity<>(responseData, HttpStatus.OK);
        } catch (Exception e) {
            responseData.setStatus(false);
            responseData.setData(null);
            responseData.setCount(0);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    public ResponseEntity<DeliveryResponse> create(@Valid @RequestBody DeliveryRequest deliveryRequest) {
        Delivery delivery = modelMapper.map(deliveryRequest, Delivery.class);
        try {
            Delivery _delivery = deliveryService
                    .save(delivery);
            DeliveryResponse deliverySuccess = modelMapper.map(_delivery, DeliveryResponse.class);
            return new ResponseEntity<>(deliverySuccess, HttpStatus.CREATED);
        } catch (Exception e) {
            DeliveryResponse deliveryFailed = modelMapper.map(deliveryRequest, DeliveryResponse.class);
            return new ResponseEntity<>(deliveryFailed, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Delivery> update(@PathVariable("id") Integer id, @Valid @RequestBody DeliveryRequest deliveryRequest) {
        Delivery data = deliveryService.findById(id);
        if (data != null) {
            return new ResponseEntity<>(deliveryRepository.save(data), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> deleteDelivery(@PathVariable("id") Integer id) {
        try {
            deliveryRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
