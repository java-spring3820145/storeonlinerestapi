package com.example.StoreOnlineRestAPI.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.StoreOnlineRestAPI.Entity.Address;
import com.example.StoreOnlineRestAPI.model.AddressRequest;
import com.example.StoreOnlineRestAPI.model.AddressResponse;
import com.example.StoreOnlineRestAPI.model.ResponseData;
import com.example.StoreOnlineRestAPI.model.ResponseDataPaging;
import com.example.StoreOnlineRestAPI.service.AddressService;

import io.swagger.v3.oas.annotations.tags.Tag;
// import jakarta.validation.Valid;
import jakarta.validation.Valid;

@RestController
@CrossOrigin(origins = "localhost:7070")
@RequestMapping("/api/address")
@Tag(name = "Address", description = "Data Address")
@Validated
public class AddressController {
    @Autowired
    private AddressService addressService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("")
    public ResponseEntity<ResponseData<List<Address>>> getData(
            @RequestParam(required = false, defaultValue = "") String keyword,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit) {
        ResponseDataPaging<List<Address>> responseData = new ResponseDataPaging<>();

        try {
            Pageable pageable = PageRequest.of(page - 1, limit);
            Page<Address> data = addressService.findPagingByParams(keyword, pageable);

            responseData.setStatus(true);
            responseData.setData(data.getContent());
            responseData.setCount(data.getNumberOfElements());
            responseData.setCurrentPage(page);
            responseData.setTotalPage(data.getTotalPages());
            return new ResponseEntity<>(responseData, HttpStatus.OK);

        } catch (Exception e) {
            responseData.setStatus(false);
            responseData.setMessage(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AddressResponse> getDataById(@PathVariable("id") String id) {
        try {
            Address address = addressService.findById(id);
            AddressResponse data = modelMapper.map(address, AddressResponse.class);
            if (data != null)
                return new ResponseEntity<AddressResponse>(data, HttpStatus.OK);
            else
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }
    }

    @PostMapping("")
    public ResponseEntity<ResponseData<AddressResponse>> create(
            @Valid @RequestBody AddressRequest addressRequest,
            Errors errors) {
        ResponseData<AddressResponse> responseData = new ResponseData<>();
        System.out.println("error");
        System.out.println(errors.hasErrors());
        if (errors.hasErrors()) {
            responseData.setStatus(false);
            // responseData.setErrors(HandleErrorException.parse(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        // try {

        // Address address = new Address();

        // address = addressService.save(addressRequest);

        // AddressResponse data = modelMapper.map(address, AddressResponse.class);
        // responseData.setStatus(true);
        // responseData.setData(data);
        // responseData.getMessage().add("Data berhasil disimpan");
        // return ResponseEntity.ok(responseData);
        // } catch (Exception e) {
        // responseData.setStatus(false);
        // responseData.getMessage().add(e.getMessage());
        // return
        // ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);

        // }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);

    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<AddressResponse>> update(
            @PathVariable("id") String id,
            @Valid @RequestBody AddressRequest addressRequest,
            Errors errors) {
        ResponseData<AddressResponse> responseData = new ResponseData<>();
        if (errors.hasErrors()) {
            responseData.setStatus(false);
            responseData.setData(null);
            // responseData.setMessage(HandleErrorException.parse(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        try {

            Address address = new Address();

            address = addressService.update(id, addressRequest);

            AddressResponse data = modelMapper.map(address, AddressResponse.class);
            responseData.setStatus(true);
            responseData.setData(data);
            responseData.setMessage("Data berhasil disimpan");
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            responseData.setStatus(false);
            responseData.setMessage(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);

        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") String id) {
        ResponseData<AddressResponse> responseData = new ResponseData<>();

        try {
            addressService.removeOne(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            responseData.setStatus(false);
            responseData.setMessage(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
