package com.example.StoreOnlineRestAPI.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.StoreOnlineRestAPI.Entity.Category;
import com.example.StoreOnlineRestAPI.repository.CategoryRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class CategoryService {
    @Autowired
    private CategoryRepository cateogryRepository;

    public Page<Category> findPagingByParams(String keyword,Pageable pageable){
        return cateogryRepository.findByNameContains(keyword, pageable);
    }

    public Category save(Category category) {
        return cateogryRepository.save(category);
    }

    public Category findById(Integer id) {
        Optional<Category> category = cateogryRepository.findById(id);
        if (!category.isPresent())
            return null;
        return cateogryRepository.findById(id).get();
    }

    public Iterable<Category> findAll() {
        return cateogryRepository.findAll();
    }

    public void removeOne(Integer id) {
        cateogryRepository.deleteById(id);
    }
}
