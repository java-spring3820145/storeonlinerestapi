package com.example.StoreOnlineRestAPI.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.StoreOnlineRestAPI.Entity.Role;
import com.example.StoreOnlineRestAPI.repository.RoleRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    public Page<Role> findPagingByParams(String keyword,Pageable pageable){
        return roleRepository.findByNameContains(keyword, pageable);
    }

    public Role save(Role role) {
        return roleRepository.save(role);
    }

    public Role findById(Integer id) {
        Optional<Role> role = roleRepository.findById(id);
        if (!role.isPresent())
            return null;
        return roleRepository.findById(id).get();
    }

    public Iterable<Role> findAll() {
        return roleRepository.findAll();
    }

    public void removeOne(Integer id) {
        roleRepository.deleteById(id);
    }
  
}
