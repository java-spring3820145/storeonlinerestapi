package com.example.StoreOnlineRestAPI.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.StoreOnlineRestAPI.Entity.AddressType;
import com.example.StoreOnlineRestAPI.repository.AddressTypeRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class AddressTypeService {
    @Autowired
    private AddressTypeRepository addressTypeRepository;

    public Page<AddressType> findPagingByParams(String keyword, Pageable pageable) {
        return addressTypeRepository.findByNameContains(keyword, pageable);
    }

     public AddressType save(AddressType addressType) {
        return addressTypeRepository.save(addressType);
    }

    public AddressType findById(Integer id) {
        Optional<AddressType> addressType = addressTypeRepository.findById(id);
        if (!addressType.isPresent())
            return null;
        return addressType.get();
    }

    public Iterable<AddressType> findAll(){
        return addressTypeRepository.findAll();
    }

    public void removeOne(Integer id){
        addressTypeRepository.deleteById(id);
    }

}
