package com.example.StoreOnlineRestAPI.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.StoreOnlineRestAPI.Entity.Division;
import com.example.StoreOnlineRestAPI.repository.DivisionRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class DivisionService {
    @Autowired
    private DivisionRepository divisionRepository;

    public Page<Division> findPagingByParams(String keyword,Pageable pageable){
        return divisionRepository.findByNameContains(keyword, pageable);
    }

    public Division save(Division division) {
        return divisionRepository.save(division);
    }

    public Division findById(Integer id) {
        Optional<Division> division = divisionRepository.findById(id);
        if (!division.isPresent())
            return null;
        return divisionRepository.findById(id).get();
    }

    public Iterable<Division> findAll() {
        return divisionRepository.findAll();
    }

    public void removeOne(Integer id) {
        divisionRepository.deleteById(id);
    }
  
}
