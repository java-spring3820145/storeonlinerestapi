package com.example.StoreOnlineRestAPI.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.StoreOnlineRestAPI.Entity.Delivery;
import com.example.StoreOnlineRestAPI.repository.DeliveryRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class DeliveryService {
    @Autowired
    private DeliveryRepository deliveryRepository;

    public Page<Delivery> findPagingByParams(String keyword,Pageable pageable){
        return deliveryRepository.findByNameContains(keyword, pageable);
    }

    public Delivery save(Delivery delivery) {
        return deliveryRepository.save(delivery);
    }

    public Delivery findById(Integer id) {
        Optional<Delivery> delivery = deliveryRepository.findById(id);
        if (!delivery.isPresent())
            return null;
        return deliveryRepository.findById(id).get();
    }

    public Iterable<Delivery> findAll() {
        return deliveryRepository.findAll();
    }

    public void removeOne(Integer id) {
        deliveryRepository.deleteById(id);
    }
  
}
