package com.example.StoreOnlineRestAPI.service;

import java.util.Optional;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.StoreOnlineRestAPI.Entity.Address;
import com.example.StoreOnlineRestAPI.Entity.AddressType;
import com.example.StoreOnlineRestAPI.model.AddressRequest;
import com.example.StoreOnlineRestAPI.repository.AddressRepository;
import com.example.StoreOnlineRestAPI.repository.AddressTypeRepository;

import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;

@Service
@Transactional
public class AddressService {

    @Autowired
    private AddressRepository addressRepositoary;

    @Autowired
    private AddressTypeRepository addressTypeRepositoary;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Validator validator;

    public Page<Address> findPagingByParams(String keyword, Pageable pageable) {
        return addressRepositoary.findByNameContains(keyword, pageable);
    }

    public Address save(AddressRequest request) {
        Set<ConstraintViolation<AddressRequest>> constrainViloation = validator.validate(request);
        Address address = modelMapper.map(request, Address.class);
        AddressType addressType = new AddressType();

        if (constrainViloation.size() > 0) {
            throw new ConstraintViolationException(constrainViloation);
        }

        if (!request.getAddress_type_id().equals(null)) {
            addressType = modelMapper.map(addressTypeRepositoary.findById(request.getAddress_type_id()),
                    AddressType.class);
            System.out.println(addressType.getId());
            System.out.println("addre ");
            System.out.println(request.getAddress_type_id());

            addressType.setName(addressType.getName());
            return addressRepositoary.save(address);

        } else {
            throw new ConstraintViolationException(constrainViloation);
        }

    }

    public Address update(String id, AddressRequest request) {

        try {
            AddressType addressType = new AddressType();
            addressType = modelMapper.map(addressTypeRepositoary.findById(request.getAddress_type_id()),
                    AddressType.class);

            Address address = this.findById(id);

            if (!addressType.getId().equals(null)) {
                address.setAddressType(addressType);
            }

            if (!address.getId().equals(null)) {
                address.setName(request.getName());
                addressRepositoary.save(address);
            }
            return null;

        } catch (Exception e) {
            return null;
        }
    }

    public Address findById(String id) {
        Optional<Address> address = addressRepositoary.findById(id);
        if (!address.isPresent())
            return null;
        return addressRepositoary.findById(id).get();
    }

    public Iterable<Address> findAll() {
        return addressRepositoary.findAll();
    }

    public void removeOne(String id) {
        addressRepositoary.deleteById(id);
    }

}
