package com.example.StoreOnlineRestAPI.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class AddressRequest {
    @NotBlank(message = "Data Required")
    private String name;
    
    @NotBlank(message = "Data Required")
    @Positive(message = "Data Must be Positive")
    private Integer address_type_id;
}
