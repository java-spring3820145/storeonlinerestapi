package com.example.StoreOnlineRestAPI.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ResponseDataPaging<T> extends ResponseData<T> {

    private Integer count;
    private Integer currentPage;
    private Integer totalPage;

}
