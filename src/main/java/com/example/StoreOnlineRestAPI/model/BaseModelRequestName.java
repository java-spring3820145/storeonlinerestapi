package com.example.StoreOnlineRestAPI.model;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class BaseModelRequestName {
    @NotEmpty(message = "name is required")
    private String name;
}
