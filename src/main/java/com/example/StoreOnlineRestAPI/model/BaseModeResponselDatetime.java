package com.example.StoreOnlineRestAPI.model;

import jakarta.persistence.MappedSuperclass;
import lombok.Data;

@Data
@MappedSuperclass
public class BaseModeResponselDatetime {
    private String created_at;
    private String updated_at;
}
