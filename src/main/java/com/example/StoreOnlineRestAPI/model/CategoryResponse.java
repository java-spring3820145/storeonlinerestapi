package com.example.StoreOnlineRestAPI.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)

public class CategoryResponse extends BaseModelReponseName {
    
}
