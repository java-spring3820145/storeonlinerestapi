package com.example.StoreOnlineRestAPI.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseData<T> {

    private boolean status;
    private List<T> errors;
    private String message;
    private T data;

}
