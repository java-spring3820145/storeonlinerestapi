package com.example.StoreOnlineRestAPI.model;

import java.sql.Timestamp;

import com.example.StoreOnlineRestAPI.Entity.AddressType;

import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
@Setter
@Getter
public class AddressResponse {
    private String id;

    private String text;

    @ManyToOne
    private AddressType addressType;

    private Integer address_type_id;

    private Timestamp updated_at;

    private Timestamp created_at;

}
