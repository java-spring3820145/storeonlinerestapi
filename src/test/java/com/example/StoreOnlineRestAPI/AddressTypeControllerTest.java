package com.example.StoreOnlineRestAPI;

import static org.junit.jupiter.api.Assertions.assertEquals;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;
import com.example.StoreOnlineRestAPI.model.AddressTypeRequest;
import com.example.StoreOnlineRestAPI.model.ResponseData;

import static org.springframework.test.web.servlet.MockMvcBuilder.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.junit.jupiter.api.BeforeEach;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

@SpringBootTest
@AutoConfigureMockMvc

public class AddressTypeControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void TestPost(){
        AddressTypeRequest request =new AddressTypeRequest();
        request.setName("unit test");

        // mockMvc.perform(
        //         post("/api/address-types")
        //                 .accept(MediaType.APPLICATION_JSON)
        //                 .contentType(MediaType.APPLICATION_JSON)
        //                 .content(objectMapper.writeValueAsString(request))
        // ).andExpectAll(
        //         status().isOk()
        // );
        // .andDo(result -> {
        //     ResponseData<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() { });

        //     assertEquals("OK", response.getData());
        // });
    }
}
